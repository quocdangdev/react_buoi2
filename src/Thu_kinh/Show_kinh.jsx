import React, { Component } from 'react'
import "./Show.css";

export default class Show_kinh extends Component {
    render() {
        let { url, price, name, desc } = this.props.detail
        return (
            <div className='container'>
                <img className='con' height="150px" width="220px" src={url} alt="" />
                <p>tên sản phẩm: {name}</p>
                <p> giá :{price}</p>
                <p>chi tiết: {desc}</p>
            </div>
        )
    }
}
