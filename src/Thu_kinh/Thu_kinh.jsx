import React, { Component } from 'react'
import { dataglasses } from './data_img';
import Item from './Item';
import Show_kinh from './Show_kinh';
import "./Show.css";

export default class Thu_kinh extends Component {

    state = {
        imgArr: dataglasses,
        detail: dataglasses[0],
    }
    handlechangDetail = (show) => {
        this.setState({
            detail: show,
        })
    }
    renderListGlasses = () => {
        return this.state.imgArr.map((item, index) => {
            return <Item
                handlechangDetail={this.handlechangDetail}
                data={item} key={index} />
        })
    }

    render() {
        console.table(this.state.imgArr);
        return (
            <div className='container bg'>
                <div className="row">{this.renderListGlasses()}</div>
                <Show_kinh detail={this.state.detail}
                />
                <img className='cha' src="./glassesImage/model.jpg" alt="" />
                {/* <Show_model detail={this.state.detail} /> */}
            </div>
        );


    }
}
