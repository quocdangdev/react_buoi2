import logo from './logo.svg';
import './App.css';
import Thu_kinh from './Thu_kinh/Thu_kinh';

function App() {
  return (
    <div className="App">
      <Thu_kinh />
    </div>
  );
}

export default App;
